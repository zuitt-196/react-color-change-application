import React from 'react'

//  use the detructue {} with the prosp passing the data to other components
const Squares = ({ colorValue, hexValue, isDarkText }) => {
    return (
        <section className="sqaure"
            // set value by using the JSX backgroundColor with the value of the colorValue from the useStatte hooks
            style={{
                backgroundColor: colorValue,
                color: isDarkText ? "#000" : "#FFF"
            }}
        >
            {/* Ternary condition high order function   */}
            <p>{colorValue ? colorValue : "Empty Value"}</p>
            <p>{hexValue ? hexValue : null}</p>

        </section>


    )
}

// use the default prosp 

Squares.defaultProps = {
    colorValue: "Empty Color Value"

}
export default Squares