import Squares from './Squares';
import Input from './Input';
import { useState } from 'react';

function App() {
  // define the useState 
  const [colorValue, setcolorValue] = useState('');

  // define the new useState the hex value color
  const [hexValue, setHexValue] = useState('');

  // Define the new value of 
  const [isDarkText, setIsDarkText] = useState(true);


  return (
    <div className="App">
      <Squares
        colorValue={colorValue}
        hexValue={hexValue}
        isDarkText={isDarkText}
      />

      <Input
        colorValue={colorValue}
        setcolorValue={setcolorValue}
        setHexValue={setHexValue}
        isDarkText={isDarkText}
        setIsDarkText={setIsDarkText}
      />
    </div>
  );
}

export default App;
